import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
// import {createStore} from 'redux';
// import  allReducers from './reducers';
import App from './components/app';
import Login from './components/login';
import Signup from './components/signup';
import Nav from './components/nav_bar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import 'font-awesome/scss/font-awesome.scss'
import './script'
import Character_detail from './components/character_detail'
import Characters from './components/characters'
import InsertData from './components/insert_data'
import Not_found from './components/404'
require('../stylesheets/style.scss');

// const store= createStore(allReducers);
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <Router>
                <div>
                    <Nav/>
                    <Switch>
                        <Route exact path="/" component={App}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/signup" component={Signup}/>
                        <Route exact path="/:owner/characters/:slug" component={Character_detail}/>
                        <Route exact path="/:owner/characters/" component={Characters}/>
                        <Route exact path="/insert_data/:insert" component={InsertData}/>
                        <Route path="*" component={Not_found}/>
                    </Switch>
                </div>
        </Router>
    </Provider>, document.getElementById('root'));

