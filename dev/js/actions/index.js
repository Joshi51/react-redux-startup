const selectUser = (user)=> {
    console.log("selected: ", user.first);
    return {
        type: "USER_SELECTED",
        payload: user
    }
};

const bannerSelected = (banners)=>{
    console.log("selected: ", banners.id);
    return {
        type: "BANNER_SELECTED",
        payload: banners
    }
};

const loginTrigger = (status)=>{
    return{
        type: "TRIGGER_LOGIN",
        payload: status
    }
};
export {selectUser,bannerSelected,loginTrigger}