import React,{Component} from 'react';
import DocumentTitle from 'react-document-title';
import config from '../../../../util/config.json'

class Character_desc extends Component{
    constructor(props) {
        super(props);
        this.state = {
            post: []
        };
    }
    componentDidMount(){
        let owner= this.props.owner;
        let slug= this.props.slug;
        let url= `${config.dev.server}${owner}/characters/${slug}`;
        fetch(url).then(
            (response)=>{return response.json()}
        ).then(
            data=>{
                this.setState({post: data[0]})
            }
        );
    }
    render(){
        if(this.state.post.img){
            return(
                <DocumentTitle title={this.state.post.name}>
                    <div>
                    <section className="hero charBanner">
                        <figure><img src={this.state.post.featured} alt=""/></figure>
                    </section>
                    <section>
                        <div className="container">
                            <img src={this.state.post.img}/>
                            <div className="charDet">
                                <h2>{this.state.post.name}</h2>
                                <p>{this.state.post.about}</p>
                            </div>
                        </div>
                    </section>
                    </div>
                </DocumentTitle>
            )
        }else{
            return null
        }
    }
}

export default Character_desc;