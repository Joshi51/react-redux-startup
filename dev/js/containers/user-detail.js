import React,{Component} from 'react';
import  {connect} from 'react-redux';

class UserDetail extends Component{

    render(){
        if (!this.props.userdata){
            return(<h2>Select a user...</h2>);
        }
        return(
          <div>
              <img src={this.props.userdata.thumbnail} alt=""/>
              <h2>Name: {this.props.userdata.first} {this.props.userdata.last}</h2>
              <h3>Age: {this.props.userdata.age}</h3>
              <h3>Description: {this.props.userdata.description}</h3>
          </div>
        );
    }
}

function mapStateToProps(state) {
    return{
        userdata: state.activeUser
    }
}

export default connect(mapStateToProps)(UserDetail);
