import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import  {connect} from 'react-redux';

class Banner_list extends Component{

    render(){
        if (!this.props.bannerData){
            return null;
        }
        let background= {
            "movies": this.props.bannerData.movies.background,
            "characters": this.props.bannerData.characters.background,
            "comics": this.props.bannerData.comics.background,
            "animated": this.props.bannerData.animated.background
        }

        return(
            <section className="hero">
                <div className="hero-body">
                        <div className="columns is-mobile">
                            <div className="banner_container">
                                <img src={require('../../../images/banners/backgrounds/'+background.movies)} alt="Avatar" className="banner_image"/>
                                <div className="middle">
                                    <Link to={this.props.bannerData.movies.url} className="banner_text button is-link">Movies</Link>
                                </div>
                            </div>
                            <div className="banner_container">
                                <img src={require('../../../images/banners/backgrounds/'+background.characters)} alt="Avatar" className="banner_image"/>
                                <div className="middle">
                                    <Link to={this.props.bannerData.characters.url} className="banner_text button is-link">Characters</Link>
                                </div>
                            </div>
                            <div className="banner_container">
                                <img src={require('../../../images/banners/backgrounds/'+background.comics)} alt="Avatar" className="banner_image"/>
                                <div className="middle">
                                    <Link to={this.props.bannerData.comics.url} className="banner_text button is-link">Comics</Link>
                                </div>
                            </div>
                            <div className="banner_container">
                                <img src={require('../../../images/banners/backgrounds/'+background.animated)} alt="Avatar" className="banner_image"/>
                                <div className="middle">
                                    <Link to={this.props.bannerData.animated.url} className="banner_text button is-link">Animated</Link>
                                </div>
                            </div>
                        </div>
                </div>
            </section>
        );
    }
}

function mapStateToProps(state) {
    return{
        bannerData: state.activeBanner
    }
}

export default connect(mapStateToProps)(Banner_list)