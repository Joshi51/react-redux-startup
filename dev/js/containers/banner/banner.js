import React,{Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {bannerSelected} from "../../actions/index";
import {Link} from 'react-router-dom';

class Banner extends Component{
    constructor(){
        super();
        // this._renderBanner= this._renderBanner().bind(this);
        this.showList= this.showList.bind(this);
    }
    _renderBanner(){
        return this.props.bannerDetail.map((banner)=>{
            const image_url= require('../../../images/'+banner.image);
            return(
                <div className="column banner_circle" id={banner.img_alt+banner.id} key={banner.id}>
                    <img onClick={()=>this.props.bannerSelect(banner)} onMouseEnter={()=>this.showList(banner.img_alt,banner.id)} src={image_url} alt={banner.img_alt} className="banner_logo"/>
                    <div id={"arrow_div"+banner.id} className="hidden">
                        <div className="arrow"></div>
                        <ul className="arrow_ul">
                            {banner.movies? <li><Link to={banner.movies.url}>Movies</Link></li>:''}
                            {banner.movies? <span className="bar">|</span>:''}

                            {banner.characters? <li><Link to={banner.characters.url}>Characters</Link></li>:''}
                            {banner.characters? <span className="bar">|</span>:''}

                            {banner.comics? <li><Link to={banner.comics.url}>Comics</Link></li>:''}
                            {banner.comics? <span className="bar">|</span>:''}

                            {banner.animated? <li><Link to={banner.animated.url}>Animated Movies</Link></li>:''}
                        </ul>
                    </div>
                </div>
            )
        })
    }
    showList(alt,id){
        let arr_div= document.getElementById("arrow_div"+id);
        arr_div.className ='show';
        let full_div= document.getElementById(alt+id);
        full_div.addEventListener("mouseleave", function(  ) {arr_div.className ='hidden'});
    }

    render(){
        return(
            <section className="hero is-dark">
                <div className="hero-body">
                    <div className="has-text-centered">
                        <div className="columns is-mobile">
                            {this._renderBanner()}
                        </div>
                    </div>

                </div>
            </section>
        );
    }
}

let mapStateToProps= (state)=>{
    return{
        bannerDetail: state.banners
    }
}

let mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({bannerSelect: bannerSelected},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Banner)