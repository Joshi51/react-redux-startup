import React,{Component} from 'react';


const helpText={
    display: "none"
};

class LoginForm extends Component{
    constructor(props){
        super(props);
        this.state={
            uname:{
                val:'',
                status:''
            },
            pass:{
                val:'',
                status:''
            }
        };

        this.handleChange= this.handleChange.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);
    }
    handleChange(comp){
        let val= comp.target.value;
        let id= comp.target.id;

        let div = document.getElementById("help_"+id);
        switch(id){
            case 'uname':
                let unameRegex= /^[a-zA-Z0-9.\\-_$@*!]{3,30}$/;
                if(unameRegex.test(val)){
                    div.style.display="none";
                    this.setState({
                        uname:{
                            val:val,
                            status:'valid'
                        }
                    });
                }else{
                    div.style.display="block";
                    this.setState({
                        uname:{
                            val:val,
                            status:'invalid'
                        }
                    });
                }
                break;
            case 'pass':
                if(val.length < 5){
                    div.style.display="block";
                    this.setState({
                        pass:{
                            val:val,
                            status:'invalid'
                        }
                    });
                }else{
                    div.style.display="none";
                    this.setState({
                        pass:{
                            val:val,
                            status:'valid'
                        }
                    });
                }
                break;
        }

    }
    handleSubmit(event ){
        event.preventDefault();
        console.log(this.state);
        let unameStatus= this.state.uname.status;
        let passStatus= this.state.pass.status;
        if(unameStatus==='valid' && passStatus==='valid'){
            document.login_form.submit()
        }
    }

    render(){
        return(
            <form id="login_form" method="post" name="login_form" action="/login_auth">
                <div className="field">
                    <label className="label">Username</label>
                    <div className="control">
                        <input className="input" type="text" id="uname" name="uname" value={this.state.value} onChange={(comp)=>this.handleChange(comp)} placeholder="Username"/>
                    </div>
                    <p className="help" id="help_uname" style={helpText}>Invalid Username</p>
                </div>
                <div className="field">
                    <label className="label">Password</label>
                    <div className="control">
                        <input className="input" type="password" id="pass" name="pass" value={this.state.value} onChange={(comp)=>this.handleChange(comp)} placeholder="Password"/>
                    </div>
                    <p className="help" id="help_pass" style={helpText}>Please Enter atleast 5 Characters</p>
                </div>
                <div className="field">
                    <p className="control">
                        <button className="button is-success" onClick={(e)=>this.handleSubmit(e)}>
                            Login
                        </button>
                    </p>
                </div>
            </form>
        )}
}

export default  LoginForm;
