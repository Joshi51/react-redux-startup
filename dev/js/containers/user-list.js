import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectUser} from "../actions/index";


class  UserList extends Component{
    createListItems(){
        return this.props.userdata.map((user)=>{
            return(
                <li key={user.id} onClick={()=>this.props.selectuser(user)}>{user.first} {user.last}</li>
            );
        });
    }
    render(){
        return(
            <ul>
                {this.createListItems()}
            </ul>
        );
    }
}
function mapStateToProps(state) {
    return{
        userdata: state.users
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({selectuser: selectUser},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(UserList);
