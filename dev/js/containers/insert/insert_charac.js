import React, {Component} from 'react';
import config from '../../../../util/config.json'

class InsertCharac extends Component{
    constructor(props){
        super(props);
        this.state = {file: '',imagePreviewUrl: '',title:'',desc:'',slug:'',alter:'',owner:''};
    }
    _handleSubmit(e){
        e.preventDefault();
        let movieUrl = `${config.dev.server}api/insert/chars`;
        let headers = new Headers();
        headers.append('Accept', 'application/json'); // This one is enough for GET requests
        headers.append('Content-Type', 'application/json'); // This one sends body
        fetch(movieUrl,{
            method: "POST",
            headers : headers,
            mode: 'same-origin',
            credentials: 'include',
            body: JSON.stringify({
                title: this.state.title,
                desc: this.state.desc,
                img: this.state.img,
                slug: this.state.slug,
                owner: this.state.owner,
                alter: this.state.alter
            }),
        }).then(response => {
                console.log("RESPONSE IS:::::", response);
                this.setState({file: '',imagePreviewUrl: '',title:'',desc:'',slug:'',alter:'',owner:''});
                document.getElementById('insertForm').reset();
            })
            .catch((err)=>{
                console.log(err)
            });
    }
    _handleChange(e){
        e.preventDefault();
        let target = e.target.name;
        switch (target){
            case "title":
                this.setState({
                    title: e.target.value
                });
                break;
            case "desc":
                this.setState({
                    desc: e.target.value
                });
                break;
            case "slug":
                this.setState({
                    slug: e.target.value
                });
                break;
            case "owner":
                this.setState({
                    owner: e.target.value
                });
                break;
            case "alter":
                this.setState({
                    alter: e.target.value
                });
                break;
            case "image":
                this.setState({
                    img: e.target.value
                });
                break;
        }
    }
    render(){
        return(
            <div>
                <h1 className="form_title has-text-centered">Movies</h1>
                <form method="POST" encType="multipart/form-data" id="insertForm">
                    <div className="field is-horizontal">
                        <div className="field-body">
                            <div className="field-label is-normal">
                                <label className="label">Title</label>
                            </div>
                            <div className="field">
                                <p className="control is-expanded">
                                    <input className="input" type="text" name="title" placeholder="Title" onChange={(e)=>this._handleChange(e)}/>
                                </p>
                            </div>
                            <div className="field-label is-normal">
                                <label className="label">Slug</label>
                            </div>
                            <div className="field">
                                <p className="control is-expanded has-icons-right">
                                    <input className="input" type="text" name="slug" placeholder="Slug to create the url" onChange={(e)=>this._handleChange(e)}/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="field is-horizontal">
                        <div className="field-body">
                            <div className="field-label is-normal">
                                <label className="label">Owner</label>
                            </div>
                            <div className="field">
                                <p className="control is-expanded select">
                                    <select name="owner" onChange={(e)=>this._handleChange(e)}>
                                        <option value="DC">DC</option>
                                        <option value="Marvel">Marvel</option>
                                    </select>
                                </p>
                            </div>
                            <div className="field-label is-normal">
                                <label className="label">Alter</label>
                            </div>
                            <div className="field">
                                <p className="control is-expanded has-icons-right">
                                    <input className="input" type="text" name="alter" placeholder="Alter Ego Name" onChange={(e)=>this._handleChange(e)}/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Description</label>
                        <div className="control">
                            <textarea className="input textarea" name="desc" placeholder="Description" onChange={(e)=>this._handleChange(e)}/>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label">Image URL</label>
                        <div className="control">
                            <input className="input" name="image" placeholder="Image URL" onChange={(e)=>this._handleChange(e)}/>
                        </div>
                    </div>
                    <button className="is-success button" onClick={(e)=>this._handleSubmit(e)}>Submit</button>
                </form>
            </div>
        )
    }
}
export default InsertCharac;