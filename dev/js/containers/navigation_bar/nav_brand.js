import React, {Component} from 'react';
import  {connect} from 'react-redux';
import { Link } from 'react-router-dom'

class Nav_brand extends Component{
    nav_items(){
        return this.props.brandsData.map((brands,key)=>{
            if(brands.img){
                return(
                    <Link to={brands.href} key={key} className="navbar-item head-logo">
                        <img src={`/dev/images/logo.png`} alt={brands.description}/>
                    </Link>
                );
            }else if(brands.logo){
                return(
                    <Link key={key} to={brands.href} className="navbar-item">
                        <span className="icon" style={{color: brands.color}}>
                        <i className={"fa "+brands.logo}></i></span>
                    </Link>
                );
            }

        })
    }
    render(){
        return(
            <div className="navbar-brand">
                {this.nav_items()}
                <div className="navbar-burger burger" data-target="navMenuTransparentExample">
                    <span/>
                    <span/>
                    <span/>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return{
        brandsData: state.navbar[0].brand
    }
}

export default connect(mapStateToProps)(Nav_brand)
