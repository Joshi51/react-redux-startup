import React, {Component} from 'react';
import  {connect} from 'react-redux';
import { Link } from 'react-router-dom'

class Nav_end extends Component{
    renderItems(){
        return this.props.navEndData.end.map((end,key)=>{
            return(
                <Link className="navbar-item" to={end.href} key={key}>
                    {end.text}
                </Link>
            )
        });
    }
    render(){
        return(
            <div className="navbar-end">
                {this.renderItems()}
            </div>
        )
    }
}

function mapStateToProps(state){
    return{
        navEndData: state.loginTrigger
    }
}

export default connect(mapStateToProps)(Nav_end)
