import React, {Component} from 'react';
import  {connect} from 'react-redux';
import { Link } from 'react-router-dom'


class Nav_start extends Component{
    renderDropdowns(drop_items){
        return drop_items.map((items,key)=>{
            return(
                <Link key={key} className="navbar-item " to={items.href}>
                    {items.name}
                </Link>
            )
        })
    }
    renderItems(){
        return this.props.navStartData.map((start,key)=>{
            if(start.dropdown){
                let drop_items= start.dropdown;
            return(
                <div key={key} className="navbar-item has-dropdown is-hoverable">
                    <Link to={start.href} className="navbar-link  is-active">
                        {start.text}
                    </Link>
                    <div className="navbar-dropdown is-boxed">
                    {this.renderDropdowns(drop_items)}
                    </div>
                </div>
            )
            }else if(start.logo){
                return(
                    <Link key={key} className="navbar-item " to={start.href}>
                        <span className="icon" style={{color: start.color}}>
                        <i className={"fa "+start.logo}></i></span>
                        {start.text}
                    </Link>
                    )
            }else{
                return(
                    <Link to={start.href} key={key} className="navbar-item">{start.text}
                    </Link>
                )
            }
        });
    }
    render(){
        return(
            <div className="navbar-start">
                {this.renderItems()}
            </div>
        )
    }
}
function mapStateToProps(state){
    return{
        navStartData: state.navbar[0].menu.start
    }
}

export default connect(mapStateToProps)(Nav_start)
