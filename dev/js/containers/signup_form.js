import React from 'react';
import {connect} from 'react-redux'
import axios from 'axios';
import {loginTrigger} from '../actions/index';
import {bindActionCreators} from "redux";

class SignupForm extends React.Component{
    constructor(props){
        super(props);
        this.state={
            signup_email: {
                'val': '',
                'status': ''
            },
            signup_pass: {
                'val':'',
                'status':''
            },
            signup_repass: {
                'val':'',
                'status':''
            }
        }
    }
    handleSubmit(e){
        e.preventDefault();
        let email_status=this.state.signup_email.status ;
        let pass_status=this.state.signup_pass.status ;
        let repass_status=this.state.signup_repass.status ;
        let email = this.state.signup_email.val;
        if( email_status ==='valid' && pass_status ==='valid' && repass_status ==='valid'){
            axios.post('/api/signup_auth', {
                signup_email: this.state.signup_email.val,
                signup_pass: this.state.signup_pass.val
            })
                .then(function (response) {
                    console.log("right");
                    console.log(response);
                    let data= response.data;
                    if(data.email){
                        this.props.triggerLogin("Logout");
                    }else if(data.errmsg){
                        let err= data.errmsg;
                        if(err.indexOf("duplicate key error")){
                            document.getElementById('signup_err_msg').innerText = 'The Email Id '+email+' already exists';
                            document.getElementById('bd-patreon').classList.remove('hidden');
                        }
                    }
                }.bind(this))
                .catch(function (error) {
                    console.log("wrong");
                    console.log(error)
                });
        }else{
            if(email_status !== "valid"){
                let div = document.getElementById("signup_email_err");
                div.style.display= "block";
            }
            if(pass_status !== "valid"){
                let div = document.getElementById("signup_pass_err");
                div.style.display= "block";
            }
            if(repass_status !== "valid"){
                let div = document.getElementById("signup_repass_err");
                div.style.display= "block";
            }
        }
    }
    stateChange(comp){
        let val= comp.target.value;
        let id= comp.target.id;

        let div = document.getElementById(id+"_err");
        switch(id){
            case 'signup_email':
                if(!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(val)){
                    div.style.display="block";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'invalid'}
                        });
                }else{
                    div.style.display="none";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'valid'}}
                    )
                }
                break;
            case 'signup_pass':
                if(val.length < 5){
                    div.style.display="block";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'invalid'}}
                    )
                }else{
                    div.style.display="none";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'valid'}}
                    )
                }
                break;
            case 'signup_repass':
                if(val.length < 5){
                    div.style.display="block";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'invalid'}}
                    )
                }else if(val !== this.state.signup_pass.val){
                    div.style.display="block";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'invalid'}}
                    )
                }else{
                    div.style.display="none";
                    this.setState(
                        {[id] : {
                                'val':val,
                                'status':'valid'}}
                    )
                }
                break;
        }
    }
    render() {
        return <form method="post" id="signup_form" name="signup_form">
            <div className="field">
                <div className="bd-patreon hidden" id="bd-patreon">
                    <p className="bd-patreon-text">
                        <strong id="signup_err_msg">ERROR ERROR</strong>
                    </p>
                </div>
                <label className="label">Email</label>
                <input className="input" type="text" id="signup_email" name="signup_email" placeholder="Enter E-Mail" onChange={(comp)=>this.stateChange(comp)}/>
                <span className="help is-danger" id="signup_email_err" style={{display: 'none'}}>Error! Invalid Name</span>
                <label className="label">Password</label>
                <input className="input" type="password" id="signup_pass" name="signup_pass" placeholder="Enter Password" onChange={(comp)=>this.stateChange(comp)}/>
                <span className="help is-danger" id="signup_pass_err" style={{display: 'none'}}>Password should atleast be of 4 digits</span>
                <label className="label">Re-enter Password</label>
                <input className="input" type="password" id="signup_repass" placeholder="Retype Password" name="signup_repass" onChange={(comp)=>this.stateChange(comp)}/>
                <span className="help is-danger" id="signup_repass_err" style={{display: 'none'}}>Password should be same as above</span>
                <button type="submit" className="button is-success" onClick={(e)=>this.handleSubmit(e)}>Submit</button>
            </div>
        </form>;
    }
}
const mapStateToProps = (state)=>{
    return({
            login: state.loginTrigger
    })
};

const mapDispatchToProps = (dispatch)=>{
    return {
        triggerLogin : (status)=>{
            dispatch(loginTrigger(status))
        }
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(SignupForm);