import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import config from '../../../util/config.json'

class Character_list extends Component{
    constructor(props) {
        super(props);
        this.state = {
            chars: [],
            isLoading: false,
            title: ""
        };
        this._openModal= this._openModal.bind(this);
        this._closeModal= this._closeModal.bind(this);
    }

    componentDidMount(){
        let owner= this.props.owner;
        switch(owner){
            case "dc":
                this.setState({title : "DC Characters"});
                break;
            case "marvel":
                this.setState({title : "Marvel Characters"});
                break;
            case "all":
                this.setState({title : "Characters"});
                break;
            default:
                window.location.href = '/404';
                break;
        }

        let url = `${config.dev.server}${owner}/characters/`;
        this.setState({isLoading: true});
        fetch(url).then(
            response => response.json())
            .then((json) => {
                this.setState({chars: json.data, isLoading:false})
            });
    }
    _openModal(e){
        let modal = document.getElementById("charModal");
        modal.classList.add("is-active");
        let data = e.target.getAttribute('data-value');
        let dataSplit = data.split(',');
        let slug = dataSplit[0];
        let owner = dataSplit[1];
        let url = `${config.dev.server}${owner}/characters/${slug}`;
        fetch(url).then(
            response => response.json())
            .then((data) => {
                let modName = document.getElementById("modName");
                let modDesc = document.getElementById("modDesc");
                let modImg = document.getElementById("modImg");
                modName.innerHTML = data[0].name;
                modDesc.innerHTML = (data[0].about).substring(0,150)+ "...";
                modImg.innerHTML = '<img src="'+data[0].img+'" alt="'+data[0].name+'"/>';
            });
    }
    _closeModal(){
        let modal = document.getElementById("charModal");
        modal.classList.remove("is-active");
    }
    renderModal(){
        return(
            <div className="modal" id="charModal">
                <div className="modal-background"> </div>
                <div className="modal-content">
                    <div className="box">
                        <p><strong id="modName"> </strong></p><br/>
                            <figure id="modImg"> </figure>
                            <span id="modDesc"> </span>
                    </div>
                </div>
                <button className="modal-close is-large" aria-label="close" onClick={(e)=>this._closeModal(e)}> </button>
            </div>
        )
    }
    renderChars(){
        let is_pointer = {
            'cursor': 'pointer'
        };

        if(this.state.isLoading){
            return(<div id="preloader">
                <div id="loader"></div>
            </div>);
        }else{
            return this.state.chars.map((val,key)=>{
                let owner= val.owner.toLowerCase();
                return(
                    <div className="card brick" key={key}>
                        <header className="card-header">
                            <span className="card-header-title"><p>{val.name}</p></span>
                        </header>
                        <div className="card-content">
                            <div className="content" style={is_pointer} >
                                <figure><img data-value={val.slug+","+owner} onClick={(e)=>this._openModal(e)} src={val.img}/></figure>
                            </div>
                        </div>
                        <footer className="card-footer">
                            <Link to={"/"+owner+"/characters/"+val.slug} className="card-footer-item">Read More</Link>
                        </footer>
                    </div>
                )
            });
        }
    }
    render(){
        return(
            <DocumentTitle title={this.state.title}>
                <div className="container">
                    <div className="char_list">{this.renderChars()}</div>
                    {this.renderModal()}
                </div>
            </DocumentTitle>
        )
    }
}


export default Character_list
