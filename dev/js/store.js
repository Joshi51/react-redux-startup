import allReducers from "./reducers";
import {createStore} from "redux";
import {applyMiddleware} from "redux";
import logger from "redux-logger";

const store= createStore(
    allReducers,
    {},
    applyMiddleware(logger()));

export default store;