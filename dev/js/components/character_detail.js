import React from 'react';
import Character_desc from '../containers/content/character_desc';

const Character_detail= ({match}) => (
    <Character_desc owner={match.params.owner} slug={match.params.slug}/>
);
export default  Character_detail;