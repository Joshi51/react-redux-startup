import React from 'react';
import Nav_start from '../containers/navigation_bar/nav_start';
import Nav_end from '../containers/navigation_bar/nav_end';


const Nav_menu= () => (
    <div id="navMenuTransparentExample" className="navbar-menu">
        <Nav_start/>
        <Nav_end/>
    </div>
);
export default  Nav_menu;
