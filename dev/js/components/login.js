import React from 'react'
import LoginForm from '../containers/login_form';
import DocumentTitle from 'react-document-title';

const Login= ()=>{
    return (
        <DocumentTitle title="Login">
            <div className="container">
                <LoginForm/>
            </div>
        </DocumentTitle>
    )
};

export default Login;