import React from 'react';
import InsertCharac from '../containers/insert/insert_charac';
import DocumentTitle from 'react-document-title';

const InsertData = ({match})=>{
    return(
        <DocumentTitle title="Insert Data">
            <div className="container">
                {(() => {
                    let type = match.params.insert;
                    switch(type) {
                        case 'movies':
                            return <InsertMovie />;
                        case 'comics':
                            return <InsertComics />;
                        case 'chars':
                            return <InsertCharac />;
                        default:
                            return null;
                    }
                })()}
            </div>
        </DocumentTitle>
    )
};

export default InsertData;