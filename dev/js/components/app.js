import React from 'react';
// import UserList  from '../containers/user-list';
// import UserDetail from '../containers/user-detail'
import Banner from '../containers/banner/banner'
import Banner_list from '../containers/banner/banner_list'
import DocumentTitle from 'react-document-title';

const App= () => (
    <DocumentTitle title="Geek Paradise">
        <div className="container">
            <Banner/>
            <Banner_list/>
            {/*<h2>Username List:</h2>*/}
            {/*<UserList/>*/}
            {/*<hr/>*/}
            {/*<h2>User Details:</h2>*/}
            {/*<UserDetail/>*/}
        </div>
    </DocumentTitle>
)
export default  App;
