import React from 'react';
import SignupForm from '../containers/signup_form';
import DocumentTitle from 'react-document-title';

const Signup = ()=>{
    return (
        <DocumentTitle title="Signup">
            <div className="container">
                <SignupForm/>
            </div>
        </DocumentTitle>)
}

export default Signup