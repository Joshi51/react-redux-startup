import React from 'react';
import Nav_brand from '../containers/navigation_bar/nav_brand';
import Nav_menu from './nav_menu';

const Nav= () => (
   <div className="container">
       <nav className="navbar is-transparent">
        <Nav_brand/>
        <Nav_menu/>
       </nav>
   </div>
);
export default  Nav;
