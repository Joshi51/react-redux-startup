export default function () {
    return [
        {
            "_id": 0,
            "title": "Title 1",
            "author": "Eleanor Cervantes",
            "img": "http://media-cache-ak0.pinimg.com/736x/2e/7f/db/2e7fdb7ed765973407fed0b0141bb126.jpg",
            "content": "Qui dolor culpa ut excepteur aliquip ipsum voluptate anim adipisicing quis. Reprehenderit eiusmod sint pariatur laboris sit duis ullamco aliquip sunt. Ut ut dolor anim aute sit dolore reprehenderit exercitation ut.",
            "url": "/title1"
        },
        {
            "_id": 1,
            "title": "Title 2",
            "author": "Lilian Dickson",
            "img":"http://media-cache-ec0.pinimg.com/736x/c3/10/22/c3102281f88237e7a2515099d2e6651f.jpg",
            "content": "Elit et ea est velit sint cupidatat culpa. Sint laborum proident nisi ipsum aliqua. Aute nulla deserunt minim voluptate et quis sunt veniam. Aliquip commodo labore ex cillum. Tempor ut sit eu veniam nisi fugiat velit labore elit. Sint ipsum nostrud laborum qui enim ipsum dolor id non elit ad minim Lorem. Elit qui amet in laboris voluptate incididunt culpa esse et.",
            "url": "/title2"
        },
        {
            "_id": 2,
            "title": "Title 3",
            "author": "Sofia Sosa",
            "img":"http://media-cache-ec0.pinimg.com/600x/95/11/02/951102f18d5a50c3ca483e93c6f92f5a.jpg",
            "content": "Incididunt Lorem cupidatat tempor irure sunt dolore Lorem. Duis exercitation veniam commodo officia occaecat officia do ullamco aute. Ullamco deserunt labore tempor consectetur cupidatat et deserunt velit nisi non cillum. Commodo commodo minim dolore eu culpa duis sint laborum. Et enim incididunt aliquip nulla eiusmod. Aliqua reprehenderit proident in pariatur nostrud. Reprehenderit adipisicing magna sit sunt consequat incididunt laborum esse duis non amet ipsum id reprehenderit.",
            "url": "/title3"
        },
        {
            "_id": 3,
            "title": "Title 4",
            "author": "Acevedo Hudson",
            "content": "Dolor veniam elit dolor dolor eu. Commodo eiusmod sint proident fugiat commodo voluptate sit et nostrud. Tempor nulla ullamco do eu aute est occaecat dolor fugiat in ad consequat in.",
            "url": "/title4"
        },
        {
            "_id": 4,
            "title": "Title 5",
            "author": "Tessa Shepard",
            "img": "http://media-cache-ec0.pinimg.com/736x/0f/d0/84/0fd0847f7b48e5f16d896f62baa31b97.jpg",
            "content": "Nostrud dolore exercitation laborum incididunt enim qui deserunt ea Lorem velit duis sunt. Reprehenderit irure incididunt reprehenderit nisi culpa id culpa cupidatat sint Lorem pariatur nulla. Quis commodo labore veniam voluptate aute. Commodo cupidatat labore in reprehenderit qui laboris incididunt aliquip cillum reprehenderit est tempor. Velit est laboris duis nulla incididunt nisi est amet consequat. Lorem pariatur veniam irure do ut ad ea officia anim commodo proident.",
            "url": "/title5"
        }
    ]
}