 export default function () {
     return[
         {
             id:1,
             first: "Bucky",
             last: "Robert",
             age: 71,
             description:"Description of bucky",
             thumbnail: "https://lh3.googleusercontent.com/B4Rmc8NPG7fHIGmN65214ppzNGHNa_wuLSSJ6Dz85KJoZ0zlBFnpH16pOJBHpwA0fCs=w300"
         },
         {
             id:2,
             first: "Suresh",
             last: "Joshi",
             age: 22,
             description:"Description of Suresh",
             thumbnail: "http://enadcity.org/enadcity/wp-content/uploads/2017/02/profile-pictures.png"
         },
         {
             id:3,
             first: "Rahul",
             last: "Joshi",
             age: 21,
             description:"Description of Rahul",
             thumbnail: "http://www.gsmspain.com/foros/avatar.php?userid=293810"
         },
         {
             id:4,
             first: "Megha",
             last: "Sinha",
             age: 25,
             description:"Description of Megha",
             thumbnail: "http://www.beeblehead.com/wp-content/uploads/2014/08/FacebookNoProfile.gif"
         }
     ]
 }