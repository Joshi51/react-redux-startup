export default  ()=> {
    return [
        {
            brand: [
                {
                    href: "/",
                    img: "fevicon-300x300.png",
                    description: "home-logo"
                }
            ],
            menu: {
                start: [
                    {
                        href: "/",
                        text: "Insert",
                        dropdown:[
                            {
                                name:"Movies",
                                href:"/insert_data/movies"
                            },{
                                name:"Characters",
                                href:"/insert_data/chars"
                            },{
                                name:"Comics",
                                href:"/insert_data/comics"
                            }
                        ]
                    }
                ]
            }
        }
    ]
}
