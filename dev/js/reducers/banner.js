export default function () {
    return[
        {
            id:1,
            image: "dc_logo.jpg",
            img_alt: "dc_logo",
            name: "DC",
            movies: {
                    url: "/dc/movies",
                    background: "dc-movies-300x300.jpg"
                    },
            characters: {
                url: "/dc/characters",
                background: "dc-charachers-300x300.jpg"
            },
            comics: {
                url: "/dc/comics",
                background: "dc-comics-300x300.jpg"
            },
            animated: {
                url: "/dc/animated",
                background: "dc-animated-300x300.jpg"
            }
        },
        {
            id:2,
            image: "marvel-logo-square.jpg",
            img_alt: "marvel_logo",
            name: "Marvel",
            movies: {
                url: "/marvel/movies",
                background: "marvel-movies-300x300.jpg"
            },
            characters: {
                url: "/marvel/characters",
                background: "marvel-characters-300x300.jpg"
            },
            comics: {
                url: "/marvel/comics",
                background: "marvel-comics-300x300.jpg"
            },
            animated: {
                url: "/marvel/animated",
                background: "marvel-animated-300x300.jpg"
            }
        }
    ]
}