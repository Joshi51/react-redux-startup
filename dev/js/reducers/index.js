import {combineReducers} from 'redux';

import  UserReducer from './users';
import ActiveUserReducer from './active-users'
import LoginReducer from './login-trigger'
import nav_menu from "./nav_bar";
import BlogsReducer from "./blogs";
import Banners from "./banner";
import ActiveBanner from './active-banner';

const allReducers  = combineReducers({
   users: UserReducer,
   activeUser: ActiveUserReducer,
   navbar: nav_menu,
   blogs_list: BlogsReducer,
   banners: Banners,
   activeBanner:ActiveBanner,
   loginTrigger: LoginReducer
});

export default allReducers;
