![](http://i.imgur.com/DUiL9yn.png)

# React/Sass/Redux Boilerplate

Boilerplate and guide for a React/Sass/Redux build.

## Getting Started

To get started, first install all the necessary dependencies.
```
> npm install
```
Make sure you have mongodb setup already(some tables are created manually like the content add them according to schemas in api/models)

Run an initial webpack build
```
> webpack (install webpack globally)
```

Start the development server (changes will now update live in browser)
```
> npm run dev
```

To view your project, go to: [http://localhost:3000/]


#PREREQUISITE

> MongoDB configured (DB used is `test` which is created by mongodb no need to create it)

> To set Counters
    > run 'mongo' command in terminal
    > run 'use test' command
    > run 'db.counters.insert({ _id: 'chars', seq: 1 })' command

> Start by creating a character on http://localhost:3000/insert_data/chars