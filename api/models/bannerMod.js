'use strict';

let mongoose= require('mongoose');
let Schema= mongoose.Schema;

let bannerSchema= new Schema({
    id: {type:String, required:true},
    image: String,
    img_alt: String,
    name: String,
    movies: [{ background: String, url: String }],
    characters: [{ background: String, url: String }],
    comics: [{ background: String, url: String }],
    animated: [{ background: String, url: String }]
},{collection: 'banner_list'});

module.exports= mongoose.model('Banners',bannerSchema);
