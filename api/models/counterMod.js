'use strict';

let mongoose= require('mongoose');
let Schema= mongoose.Schema;

let counterSchema= new Schema({
    _id: String,
    seq: Number
},{collection: 'counters'});

module.exports= mongoose.model('counters',counterSchema);