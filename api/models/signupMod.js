'use strict';

let mongoose= require('mongoose'),
    Schema= mongoose.Schema;

let signupSchema= new Schema({
    email: {type:String, required:true, unique : true, dropDups: true},
    pass: String
},{collection: 'user_details'});

module.exports= mongoose.model('Signup',signupSchema);