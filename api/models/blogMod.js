'use strict';

let mongoose= require('mongoose');
let Schema = mongoose.Schema;

let blogSchema= new Schema({
    id: {type:String, required:true},
    title: String,
    content: String
});

module.exports = mongoose.model('Blogs', blogSchema);