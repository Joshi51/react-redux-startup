'use strict';

let mongoose= require('mongoose');
let Schema= mongoose.Schema;

let charSchema= new Schema({
    id: {type:Number, required:true},
    name: String,
    about: String,
    slug: String,
    owner: String,
    img: String,
    alter: String,
    featured: String
},{collection: 'character_detail'});

module.exports= mongoose.model('Characters',charSchema);