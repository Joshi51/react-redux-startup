'use strict';


let mongoose = require('mongoose'),
    Blogs = mongoose.model('Blogs');

exports.blogData = function(req, res) {
    Blogs.find({}, function(err, blogs) {
        if (err)
            res.send(err);
        res.json(blogs);
    });
};