'use strict';
let util = require('../../util');

let mongoose =require('mongoose'),
    Characters= mongoose.model('Characters');

exports.characterData= (req,res)=>{
    let owner= req.params.owner;

    switch (owner){
        case "all":
            Characters.find({}, (err, chars)=>{
                if (err)
                    util.errorHandler(400, err, res)//res.send(err);
                util.sendResponse(200, chars, res)//res.json(chars);
            });
            break;
        case "marvel":
            Characters.find({"owner":"Marvel"}, (err, chars)=>{
                if (err)
                    util.errorHandler(400, err, res)//res.send(err);
                util.sendResponse(200, chars, res)//res.json(chars);
            });
            break;
        case "dc":
            Characters.find({"owner":"DC"}, (err, chars)=>{
                if (err)
                    util.errorHandler(400, err, res)//res.err(err);
                util.sendResponse(200, chars, res) //res.json(chars);
            });
            break
    }
};

exports.selCharData= (req,res)=>{
    let slug= req.params.slug;

    Characters.find({"slug":slug}, (err, char)=>{
        if (err)
            res.send(err);
        res.json(char);
    });
}