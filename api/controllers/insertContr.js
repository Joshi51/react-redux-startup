'use strict';
let mongoose = require('mongoose'),
    Characters= mongoose.model('Characters'),
    Counters = mongoose.model('counters');
    // charModel = mongoose.model('charInsert');
    // comicsModel = mongoose.model('comicsInsert');


exports.insertData= (req,res)=>{
    let owner= req.params.insert;

    switch (owner){
        case "movies":
            return "movies";
            break;
        case "chars":
            let query   = { _id: "chars" };
            let update  = { $inc: {seq: 1} };
            let options = { new: true };
            Counters.findOneAndUpdate(query, update, options, function(err, doc){
                if(err){console.log("error",err)}else{
                    let data = new Characters({
                        id: doc.seq,
                        name: req.body.title,
                        about: req.body.desc,
                        slug: req.body.slug,
                        img: req.body.img,
                        owner: req.body.owner,
                        alter: req.body.alter
                    });
                    Characters.init().then(
                        Characters.create(data, (err,data)=>{
                            if(err){
                                res.json(err)
                            }else{
                                res.json(true)
                            }
                        })
                    );
                }
            });
            break;
        case "comics":
            return "comics";
            break
    }
};