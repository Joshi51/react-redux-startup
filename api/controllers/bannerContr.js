'use strict';

let mongoose = require('mongoose'),
    Banners = mongoose.model('Banners');

exports.bannerData = (req, res)=>{
    Banners.find({}, (err, banners)=>{
        if (err)
            res.send(err);
        res.json(banners);
    });
};