'use strict';

let mongoose =require('mongoose'),
    Signup= mongoose.model('Signup'),
    sessionData,
    bcrypt= require('bcrypt-nodejs');

exports.signupData= (req,res)=>{
    sessionData= req.session;
    sessionData.signup_email= req.body.signup_email;
    let hash = bcrypt.hashSync(req.body.signup_pass);
    let userData= new Signup({
        email: sessionData.signup_email,
        pass: hash
    });

    Signup.init().then(function() {
        Signup.create(userData, function(error,savedData) {
            if(error){
                res.json(error)
            }else{
                let dataToReturn= {};
                dataToReturn.email= savedData.email;
                dataToReturn.loginStatus= true;
                res.json(dataToReturn)
            }
        });
    });
};