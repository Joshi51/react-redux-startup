'use strict';

module.exports = (app)=>{
    let bannerContr = require('../controllers/bannerContr');

    app.route('/api/banner_list')
        .get(bannerContr.bannerData)
};