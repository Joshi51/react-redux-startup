'use strict';

module.exports = (app)=>{
    let charContr= require('../controllers/characterContr');

    app.route('/api/:owner/characters/')
        .get(charContr.characterData);

    app.route('/api/:owner/characters/:slug')
        .get(charContr.selCharData);
};