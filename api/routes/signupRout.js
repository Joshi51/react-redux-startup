'use strict';

module.exports = (app)=>{
    let signupContr= require('../controllers/signupContr');

    app.route('/api/signup_auth')
        .post(signupContr.signupData);
};