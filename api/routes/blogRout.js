'use strict';

module.exports = (app)=>{
    let blogContr = require('../controllers/blogContr');

    app.route('/api/blogs')
        .get(blogContr.blogData);
};