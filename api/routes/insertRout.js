'use strict';

module.exports = (app)=>{
    let insertContr= require('../controllers/insertContr');
    app.route('/api/insert/:insert')
        .post(insertContr.insertData);
};