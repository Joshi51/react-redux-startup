let path = require("path"),
    express = require("express"),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    webpack = require('webpack'),
    config = require('./webpack.config'),
    multer = require('multer');

let mongoose= require('mongoose'),
    Blogs = require('./api/models/blogMod'),
    Banners = require('./api/models/bannerMod'),
    Characters = require('./api/models/characterMod'),
    Signup = require('./api/models/signupMod'),
    Counters = require('./api/models/counterMod');

mongoose.connect('mongodb://localhost:27017/test', { useMongoClient: true });

let DIST_DIR = path.join(__dirname, "src"),
    PORT = 3000,
    app = express(),
    compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true, publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

//Serving the files on the dist folder
app.use(express.static(DIST_DIR));

app.use(session({
    secret: "session initialized",
    name: "secretCookie",
    proxy: true,
    resave: false,
    saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let blogRoute = require('./api/routes/blogRout'); //importing route
blogRoute(app); //register the route

let bannerRoute = require('./api/routes/bannerRout'); //importing route
bannerRoute(app); //register the route

let characterRoute = require('./api/routes/characterRout'); //importing route
characterRoute(app); //register the route

let signupRoute = require('./api/routes/signupRout'); //importing route
signupRoute(app); //register the route

let insertRoute = require('./api/routes/insertRout'); //importing route
insertRoute(app); //register the route

let Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./dev/images/temp");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});
let upload = multer({
    storage: Storage
}).array("imgUploader", 3); //Field name and max count
app.post("/api/Upload/", (req,res)=>{
    upload(req, res, function(err) {
        if (err) {
            return res.end("Something went wrong!");
        }
        return res.end("File uploaded sucessfully!.");
    });
});

app.get("*", (req, res)=>{
    res.sendFile(path.join(DIST_DIR, "index.html"));
});


app.listen(PORT);
console.log('server runnung at'+PORT);