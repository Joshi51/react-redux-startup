exports.sendResponse = (statusCode, msg, res) => {
    res.status(statusCode).send({
        status: statusCode,
        data: msg,
    })
};

exports.errorHandler = (statusCode, err, res) => {
    console.log("ERROR", err);
    res.status(statusCode).send({
        status: statusCode,
        data: err.message,
    })
};